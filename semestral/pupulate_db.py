from app import app, db, Author, Book, Publisher

# Use the application context to work with the database
with app.app_context():
    # Drop all tables and create them fresh
    db.drop_all()
    db.create_all()

    # Create sample publishers
    publisher1 = Publisher(name='Penguin Random House')
    publisher2 = Publisher(name='HarperCollins')

    # Create sample authors
    author1 = Author(name='J.K. Rowling')
    author2 = Author(name='George R.R. Martin')
    author3 = Author(name='J.R.R. Tolkien')

    # Create sample books
    book1 = Book(title='Harry Potter and the Philosopher\'s Stone', publisher=publisher1)
    book2 = Book(title='Harry Potter and the Chamber of Secrets', publisher=publisher1)
    book3 = Book(title='A Game of Thrones', publisher=publisher2)
    book4 = Book(title='A Clash of Kings', publisher=publisher2)
    book5 = Book(title='The Hobbit', publisher=publisher1)
    book6 = Book(title='The Lord of the Rings', publisher=publisher1)

    # Associate authors with books (many-to-many relationship)
    book1.authors.append(author1)
    book2.authors.append(author1)
    book3.authors.append(author2)
    book4.authors.append(author2)
    book5.authors.append(author3)
    book6.authors.append(author3)

    # Add all entities to the session and commit them to the database
    db.session.add_all([publisher1, publisher2, author1, author2, author3, book1, book2, book3, book4, book5, book6])
    db.session.commit()

    print("Database populated with initial data.")
