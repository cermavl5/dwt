from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Api, Resource
from flask_marshmallow import Marshmallow
from flask_swagger_ui import get_swaggerui_blueprint

app = Flask(__name__)

# Configure database
app.config['SQLALCHEMY_DATABASE_URI'] = 'mariadb+mariadbconnector://cermavl5_dwt:password@7ff67a253f86:3306/DWT'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
ma = Marshmallow(app)
api = Api(app)

# Swagger setup
SWAGGER_URL = '/swagger'
API_URL = '/static/swagger.json'
swaggerui_blueprint = get_swaggerui_blueprint(SWAGGER_URL, API_URL, config={'app_name': "Flask-SQLAlchemy-API"})
app.register_blueprint(swaggerui_blueprint, url_prefix=SWAGGER_URL)


# Create database models
class Musician(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    instruments = db.relationship('Instrument', secondary='musician_instrument', backref='musicians')


class Instrument(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    brand_id = db.Column(db.Integer, db.ForeignKey('brand.id'), nullable=False)


class Brand(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    instruments = db.relationship('Instrument', backref='brand', lazy=True)


class MusicianInstrument(db.Model):
    __tablename__ = 'musician_instrument'
    musician_id = db.Column(db.Integer, db.ForeignKey('musician.id'), primary_key=True)
    instrument_id = db.Column(db.Integer, db.ForeignKey('instrument.id'), primary_key=True)


# Create Marshmallow schemas
class MusicianSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Musician


class InstrumentSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Instrument


class BrandSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Brand


musician_schema = MusicianSchema()
musicians_schema = MusicianSchema(many=True)
instrument_schema = InstrumentSchema()
instruments_schema = InstrumentSchema(many=True)
brand_schema = BrandSchema()
brands_schema = BrandSchema(many=True)


# Create resources with CRUD operations
class MusicianResource(Resource):
    def get(self, musician_id=None):
        if musician_id:
            musician = Musician.query.get(musician_id)
            if musician:
                return musician_schema.dump(musician)
            return {'message': 'Musician not found'}, 404
        musicians = Musician.query.all()
        return musicians_schema.dump(musicians)

    def post(self):
        new_musician = Musician(name=request.json['name'])
        db.session.add(new_musician)
        db.session.commit()
        return musician_schema.dump(new_musician), 201

    def put(self, musician_id):
        musician = Musician.query.get(musician_id)
        if musician:
            musician.name = request.json['name']
            db.session.commit()
            return musician_schema.dump(musician)
        return {'message': 'Musician not found'}, 404

    def delete(self, musician_id):
        musician = Musician.query.get(musician_id)
        if musician:
            db.session.delete(musician)
            db.session.commit()
            return {'message': 'Musician deleted'}
        return {'message': 'Musician not found'}, 404


class InstrumentResource(Resource):
    def get(self, instrument_id=None):
        if instrument_id:
            instrument = Instrument.query.get(instrument_id)
            if instrument:
                return instrument_schema.dump(instrument)
            return {'message': 'Instrument not found'}, 404
        instruments = Instrument.query.all()
        return instruments_schema.dump(instruments)

    def post(self):
        new_instrument = Instrument(
            name=request.json['name'],
            brand_id=request.json['brand_id']
        )
        db.session.add(new_instrument)
        db.session.commit()
        return instrument_schema.dump(new_instrument), 201

    def put(self, instrument_id):
        instrument = Instrument.query.get(instrument_id)
        if instrument:
            instrument.name = request.json['name']
            instrument.brand_id = request.json['brand_id']
            db.session.commit()
            return instrument_schema.dump(instrument)
        return {'message': 'Instrument not found'}, 404

    def delete(self, instrument_id):
        instrument = Instrument.query.get(instrument_id)
        if instrument:
            db.session.delete(instrument)
            db.session.commit()
            return {'message': 'Instrument deleted'}
        return {'message': 'Instrument not found'}, 404


class BrandResource(Resource):
    def get(self, brand_id=None):
        if brand_id:
            brand = Brand.query.get(brand_id)
            if brand:
                return brand_schema.dump(brand)
            return {'message': 'Brand not found'}, 404
        brands = Brand.query.all()
        return brands_schema.dump(brands)

    def post(self):
        new_brand = Brand(name=request.json['name'])
        db.session.add(new_brand)
        db.session.commit()
        return brand_schema.dump(new_brand), 201

    def put(self, brand_id):
        brand = Brand.query.get(brand_id)
        if brand:
            brand.name = request.json['name']
            db.session.commit()
            return brand_schema.dump(brand)
        return {'message': 'Brand not found'}, 404

    def delete(self, brand_id):
        brand = Brand.query.get(brand_id)
        if brand:
            db.session.delete(brand)
            db.session.commit()
            return {'message': 'Brand deleted'}
        return {'message': 'Brand not found'}, 404


# Add resources to API with paths
api.add_resource(MusicianResource, '/musicians', '/musicians/<int:musician_id>')
api.add_resource(InstrumentResource, '/instruments', '/instruments/<int:instrument_id>')
api.add_resource(BrandResource, '/brands', '/brands/<int:brand_id>')

# Initialize database
with app.app_context():
    db.create_all()

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=63203)