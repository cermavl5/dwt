# Instructions

> ⚠️ **Warning:**
>
> USE YOUR OWN PASSWORDS, USER NAMES, PORTS AND CONTAINER NAMES


## On Debian/Ubuntu systems, you need to install the python3-venv package
```bash
# password is password
sudo apt install python3.10-venv
```

## Create and activate a virtual environment
```bash
python3 -m venv venv07
source venv07/bin/activate
```

## Instal mysql/mariadb client
```bash
sudo apt-get update -y
sudo apt-get install -y libmariadb-dev

## Install the required packages
```bash
pip3 install -r requirements.txt
```

## From docker host machine (kbi-server.fbmi.cvut.cz) attach vscode to the docker network of mariadb database

```bash
# if network does not exist, create it first
docker network create net-alice_dwt
# and also attach db container to network
docker network connect net-alice_dwt db-alice-dwt
docker network connect net-alice_dwt openvscode-server-alice
```

## In app.py change connection string to the database:
```bash
# find hostname/container id of the mariadb container
docker ps | grep db-alice-dwt
# copy the container id
```
```python
# please note that inside docker network, the port is 3306
app.config['SQLALCHEMY_DATABASE_URI'] = 'mariadb+mariadbconnector://<your_user>:<your_password>@<container_id>:3306/DWT'
```

