# Instructions

> ⚠️ **Warning:**
>
> USE YOUR OWN PASSWORDS, USER NAMES, PORTS AND CONTAINER NAMES

## prepare Dockerfile according to the sample
- change port number in the Dockerfile

## copy app from vscode server container to the host
```bash
docker cp openvscode-server-cermavl5_dwt:/path/to/source/folder /path/on/host
# docker cp openvscode-server-cermavl5_dwt:/opt/app-data/cv07/mysql_simple_app /home/cermavl5_dwt/app1

```

## build the docker image
```bash
docker build -t <your_user>t/mysql-simple-app:1.0 .
```

## run the docker container with proper network and ports
```bash
docker run -d --rm --name cermavl5_dwt-mysql-simple-app -p 63003:63003 --network net-cermavl5_dwt cermavl5_dwt/mysql-simple-app:1.0
```